# terabitSec

Só eu sei o tempo e esforço que eu coloquei no terabitSec, mas chegou ao fim.

Devido a alguns problemas pessoais do passado que voltaram recentemente, tenho que me esforçar para romper qualquer ligação que possa me causar mais problemas.

Com muita tristeza e vergonha de abandonar aqueles que realmente precisavam desse projeto, estarei liberando as [fontes dos cursos](#fontes-de-cursos) e o [código fonte](#código-fonte-do-athena) do programa que reupava esses vídeos em streaming.

Sintam-se livres para melhorar e redistribuir esse código, já que eu reconheço as minhas limitações e sei que há inúmeras formas de otimiza-lo.

Todas as doações que não foram gastas estão ressarcidas, obrigada por confiar em mim.

_\- De sua eterna skiddie, Marina von Ornstein._

<br />

**TODOS** cursos postados no terabitSec foram vazados primeiramente por [outros grupos](#fontes-de-cursos), nunca na nossa história efetuamos a primeira distribuição de um curso.

## fontes de cursos
- https://t.me/Hide01
- https://t.me/AcervoC
- https://t.me/zero0lab
- https://t.me/RedBlueHit
- https://t.me/acervoprivado

## código fonte do Athena
**OBS: Lembrem-se que o programa está sob a licensa GPL-3, quaisquer mudanças/aprimoramentos no código devem ser publicados gratuitamente para a comunidade.**

Acesse [aqui](https://git.disroot.org/terabitSec/athena) o código fonte do programa que extrai cursos comprimidos e envia pra o telegram em forma de streaming.
